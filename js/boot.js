define([], function() {
	// Underscore's throttle.
	var throttle = function(func, wait, options) {
		var context, args, result;
		var timeout = null;
		var previous = 0;
		options || (options = {});
		var later = function() {
			previous = new Date;
			timeout = null;
			result = func.apply(context, args);
		};
		return function() {
			var now = new Date;
			if (!previous && options.leading === false) previous = now;
			var remaining = wait - (now - previous);
			context = this;
			args = arguments;
			if (remaining <= 0) {
				clearTimeout(timeout);
				timeout = null;
				previous = now;
				result = func.apply(context, args);
			} else if (!timeout && options.trailing !== false) {
				timeout = setTimeout(later, remaining);
			}
			return result;
		};
	};

	var articleBodyEl;
	var mainBodyEl;
	var figureEl;
	var cssURL = 'http://interactive.guim.co.uk/2014/11/menu-interactive/css/boot.css';
	// var cssURL = 'css/boot.css';
	var throttledScroll = throttle(onScroll, 300);
	var throttledResize = throttle(onResize, 300);
	var linksEl;
	var liEls;
	var headingsEls;
	var wrapperEl;
	var previousValue = false;

	function resizeWrapper() {
		if (navEl.className.indexOf('active') === -1) {
			return false;
		}
		var contentMargin = parseInt(getComputedStyle(articleBodyEl).marginLeft);
		var contentWidth = parseInt(articleBodyEl.getBoundingClientRect().width);
		var contentOffset = articleBodyEl.getBoundingClientRect().left;
		navEl.style.width = contentMargin + contentWidth + 'px';
		navEl.style.left = contentOffset - contentMargin + 'px';
	}

	function onResize() {
		resizeWrapper();
	}

	function onScroll() {
		if (isStaticNavOutOfView()) {
			if (navEl.className.indexOf('active') === -1) {
				// Set timeout to wait for the correct height of the wrapper
				setTimeout(function() {
					var nav_wrapper = document.querySelector('.nav_wrapper');
					nav_wrapper.style.height = nav_wrapper.getBoundingClientRect().height + 'px';
					navEl.className += ' active';
					resizeWrapper();
				}, 100)
			}
		} else {
			navEl.className = navEl.className.replace(' active', '');
			navEl.className = navEl.className.replace('openNav', '');
			navEl.style.width = 'auto';
		}

		headingLinks = figureEl.querySelectorAll('a');
		for (i = 0; i < headingLinks.length; i++) {
			headingLinks[i].className = headingLinks[i].className.replace('active-link', '');
		};

		currentChapter = headingLinks[getNearestTopIndex()];
		if (currentChapter) {
			currentChapter.className += 'active-link';
		}
	}

	function isStaticNavOutOfView() {
		return (wrapperEl.getBoundingClientRect().bottom < 0 &&
			mainBodyEl.getBoundingClientRect().bottom - window.innerHeight / 2 > 0);
	}

	function addCSS(url) {
		var cssEl = document.createElement('link');
		cssEl.setAttribute('type', 'text/css');
		cssEl.setAttribute('rel', 'stylesheet');
		cssEl.setAttribute('href', url);
		var head = document.querySelector('head');
		head.appendChild(cssEl);

	}

	function jumpToHeading(event) {
		event.preventDefault();
		var targetEl = document.querySelector(this.getAttribute('href'));
		var pos = targetEl.getBoundingClientRect().top + window.pageYOffset;
		pos -= window.innerHeight / 8;
		window.scrollTo(0, pos);
	}

	function setupDOM( element ) {
		mainBodyEl = document.querySelector('.content__main-column.content__main-column--article');
		articleBodyEl = document.querySelector('.content__main-column.content__main-column--article');
		headingsEls = document.querySelectorAll('.content__article-body h2');

		navEl = document.createElement('div');
		navEl.setAttribute('id', 'article-navigation');
		navEl.className += ' article_nav';

		navigationTitle = document.createElement('h2');
		navigationTitle.innerHTML = element.getAttribute( 'data-alt' );
		navEl.appendChild(navigationTitle);

		navList = document.createElement('ol');

		var heads = document.querySelectorAll('.content__article-body h2');

		var chapterNames = Array.prototype.map.call(heads, function(el) {
			return el.innerHTML;
		});

		// Add nav IDs to the <h2> headings
		for (var i = 0; i < headingsEls.length; i++) {
			navListItem = document.createElement('li');
			headingsEls[i].setAttribute('id', 'nav' + i);
			headingsEls[i].setAttribute('name', 'nav' + i);
			var navLink = document.createElement('a');
			navLink.href = "#" + headingsEls[i].getAttribute('id');
			navLink.innerHTML = chapterNames[i];
			navLink.setAttribute('data-title', (i + 1) + ". " + chapterNames[i]);
			navLink.addEventListener('click', jumpToHeading);

			navListItem.appendChild(navLink);
			navList.appendChild(navListItem);
		}
		navEl.appendChild(navList);
		navEl.addEventListener('click', handleNavClick, false);

		// Add wrapper around <ol> so when it becomes fixed it doesn't alter
		// the page height causing jump
		wrapperEl = document.createElement('figure');
		wrapperEl.className += 'nav_wrapper element';
		wrapperEl.appendChild(navEl);
		figureEl.innerHTML = "";
		figureEl.appendChild(wrapperEl);



		// Add menu button
		var menuEl = document.createElement('div');
		menuEl.className += ' menu';
		navEl.appendChild(menuEl);

		// Load the CSS
		addCSS(cssURL);
	}

	function handleNavClick() {
		if (navEl.className.indexOf('active') === -1) {
			return false;
		}

		if (navEl.className.indexOf('openNav') === -1) {
			navEl.className += ' openNav';
		}
		else {
			navEl.className = navEl.className.replace('openNav', '');
		}
	}

	function getNearestTopIndex() {
		var nearestIndex = null;
		var tmpHeight = -100000;

		for (var i = 0; i < headingsEls.length; i++) {
			var top = headingsEls[i].getBoundingClientRect().top;
			top -= (window.innerHeight / 8) + 5;

			if (top < 0 && top > tmpHeight) {
				tmpHeight = top;
				nearestIndex = i;
			}
		}

		return nearestIndex;
	}

	function getLinkFromHeadingIndex(index) {
		var filteredLinks = headingLinks.filter(function(item) {
			return item.index === index;
		});

		// return filteredLinks[0];
	}


	function boot(el) {
		figureEl = el;
		setupDOM( el );
		throttledScroll();
		throttledResize();
		window.addEventListener('scroll', throttledScroll);
		window.addEventListener('resize', throttledResize);
	}

	return {
		boot: boot
	};
});
